//
//  ViewController.swift
//  ImageRecognition
//
//  Created by Sergey Kravtsov on 16.10.2017.
//  Copyright © 2017 Sergey Kravtsov. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import Vision
import LASwift

class ViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {

    let label: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Category"
        label.textAlignment = .center
        label.font = label.font.withSize(22)
        label.backgroundColor = UIColor.init(white: 0.0, alpha: 0.3)
        label.numberOfLines = 2
        return label
    }()
    
    let FPS: UILabel = {
        let label = UILabel()
        label.textColor = .green
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "FPS"
        label.textAlignment = .center
        label.font = label.font.withSize(19)
        label.backgroundColor = UIColor.init(white: 0.0, alpha: 0.2)
        label.numberOfLines = 1
        return label
    }()
    
    var lastTime: Double = -1.0

    override func viewDidLoad() {
        super.viewDidLoad()
        setupCaptureSession()
        view.addSubview(label)
        view.addSubview(FPS)
        setupLabel()
        
        // Loading classes
        if let filepath = Bundle.main.path(forResource: "classes", ofType: "txt") {
            do {
                let contents: String = try String(contentsOfFile: filepath)
                contents.enumerateLines { (line, _) -> () in
                    self.classes.append(line)
                }

                print("Count " + String(classes.count))
            } catch {
                print("contents could not be loaded")
            }
        } else { }
        
        // Loading vectors
        if let filepath = Bundle.main.path(forResource: "vectors", ofType: "txt") {
            do {
                var lines: [String] = []
                let contents: String = try String(contentsOfFile: filepath)
                contents.enumerateLines { (line, _) -> () in
                    lines.append(line)
                }
                
                var class_means: [[Double]] = []
                var i = 0
                for line in lines {
                    var vec: [Double] = []
                    for seq in line.components(separatedBy: " ") {
                        vec.append(Double(seq) as! Double)
                    }
                    class_means.append(vec)
                    i += 1
                }
                
                self.index = Matrix(class_means)

            } catch {
                print("Error info: \(error)")
            }
        } else { }
        
        
    }
    
    var classes: [String] = []
    var index: Matrix = Matrix([[1.0]])

}

extension ViewController {
    func setupCaptureSession() {
        
        let captureSession = AVCaptureSession()
        let availableDevices = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .back).devices
        
        do {
            if let captureDevice = availableDevices.first {
                captureSession.addInput(try AVCaptureDeviceInput(device: captureDevice))
            }
        } catch {
            print(error.localizedDescription)
        }
        
        let captureOutput = AVCaptureVideoDataOutput()
        captureOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "videoQueue"))
        captureSession.addOutput(captureOutput)
        
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.frame
        view.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let model = try? VNCoreMLModel(for: model_2_injected().model) else { return }
        let request = VNCoreMLRequest(model: model) { (finishedRequest, error) in
            guard let results = finishedRequest.results as? [VNCoreMLFeatureValueObservation] else { return }
            guard let observation = results.first else { return }
            guard let features = observation.featureValue.multiArrayValue else { return }

            var embedding: [Double] = Array(repeating: 0, count: features.count)
            for i in 0..<features.count {
                embedding[i] = Double(features[i].floatValue)
            }
            let embedding_vector = Vector(embedding)

            // Calculating closest class
            if self.index.rows == 1034 {
                let diff_sq = sum(power(self.index - embedding_vector, 2), .Row)
                let best_index = mini(diff_sq)

                DispatchQueue.main.async(execute: {
                    let comps = self.classes[best_index].components(separatedBy: " - ")
                    let result: String = comps[0] + "\n" + comps[1]
                    self.label.text = result
                })
                
                DispatchQueue.main.async(execute: {
                    let newTime: Double = CACurrentMediaTime()
                    self.FPS.text = String(format: "%.1f", 1.0 / (Double(newTime) - Double(self.lastTime))) + " FPS"
                    self.lastTime = newTime
                })
            }
        }
        guard let pixelBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }
        
        // executes request
        try? VNImageRequestHandler(cvPixelBuffer: pixelBuffer, options: [:]).perform([request])
    }
}

extension Double {
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}

extension ViewController {
    func setupLabel() {
        label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        label.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -70).isActive = true
        
        FPS.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        FPS.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -40).isActive = true
    }
}
